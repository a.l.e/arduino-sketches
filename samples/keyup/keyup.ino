#include <AsyncDelay.h>

AsyncDelay delay_200ms;

bool button_pressed = false;

const int D_IN = 16;

void setup() {
  Serial.begin(9600);

  pinMode(D_IN, INPUT);
  
  delay_200ms.start(200, AsyncDelay::MILLIS);
}

void button_up() {
  Serial.println("button up");
}

void loop() {
    if (delay_200ms.isExpired()) {
      // check for button_up for a button that has HIGH in its released state
      uint8_t pin_state = digitalRead(D_IN);
      Serial.println(pin_state);
      if (pin_state == HIGH) {
        if (button_pressed) {
          button_up();
        }
      }
      button_pressed = (pin_state == LOW);
      delay_200ms.repeat(); // Count from when the delay expired, not now
    }
}
